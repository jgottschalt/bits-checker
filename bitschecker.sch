<?xml version="1.0" encoding="UTF-8"?>

<!-- ====================================================================
	This Schematron is used to check BITS files against Brill's the specific requirements laid out on https://confluence.brill.com/display/BE/XML+and+PDF+instructions. To be used by Brill CDE.
	Copyright (C) 2022 Koninklijke Brill NV, Leiden, Netherlands.
	
	Brill BITS-Checker v1.1 © Brill / Johannes Gottschalt, August 2022
	-->
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2" xmlns:sqf="http://www.schematron-quickfix.com/validator/process" xmlns:xlink="http://www.w3.org/1999/xlink">
    
    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <!-- General checks -->
    <sch:pattern>
        <sch:rule context="//contrib-id[@contrib-id-type='orcid']">
            <sch:report test="not(@authenticated) or ./@authenticated != 'true'">orcid must have @authenticated="true"</sch:report>
            <sch:assert test="matches(., 'https://orcid.org/[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9X]')">Incorrect Orcid-Syntax.</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-id[@book-id-type='doi']|book-part-id[@book-part-id-type='doi']">
            <sch:assert test="matches(., '^10.\d{4,9}/[-._;()/:a-z0-9]+$')">
                DOI pattern incorrect
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="title-group|book-title-group">
            <sch:report test="count(./subtitle) > 1">Only one subtitle per title-group allowed. Use &lt;break/&gt; for line break in subtitle</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="trans-title-group">
            <sch:report test="count(./trans-subtitle) > 1">Only one trans-subtitle per trans-title-group allowed. Use &lt;break/&gt; for line break in subtitle</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="//*/@*">
            <sch:report test=". = ''">Empty attribute. Leave out if not required or fill correct value</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="counts">
            <sch:report test="matches(@count, '[0-9]+')">counts//@count must be numerical</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="//*/@*">
            <sch:report test=". = ''">Empty attribute. Leave out if not required or fill correct value</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="name/@name-style">
            <sch:report test=". !='eastern' and . !='western' and .!='given-only'">@name-style must be 'eastern', 'western' or 'given-only'</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book">
            <sch:assert test="./@dtd-version and ./@dtd-version='2.0'">Error! must have @dtd-version="2.0"</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book">
            <sch:assert test="./@xml:lang">Error! must have @xml:lang to specify book language</sch:assert>
        </sch:rule>
    </sch:pattern>
    <!-- /General checks -->
    
    <!-- collection-meta -->
    <sch:pattern>
        <sch:rule context="collection-meta//@contrib-type">
            <sch:assert test=" .='series editor'
                or .='editor-in-chief'
                or .='founding editor'
                or .='managing editor'
                or .='associate editor'
                or .='technical editor'
                or .='editorial board/council member'
                or .='advisory editor'
                or .='copy editor'
                or .='translator' ">Invalid contrib-type <sch:value-of select="."/>.  "series editor" (default, unless specifically identified as one of the other types on the page/by the Brill Desk Editor),"editor-in-chief", "founding editor", "managing editor", "associate editor", "technical editor", "editorial board/council member", "advisory editor", "copy editor", "translator" </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="collection-meta">
            <sch:assert test="./@collection-type">Error! missing attribute @collection-type</sch:assert>
        </sch:rule>
        <sch:rule context="collection-meta/@collection-type">
            <sch:assert test=".='series' or .='sub-series'">Error! Incorrect value for @collection-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="collection-meta[@collection-type = 'series']">
            <sch:assert test="./collection-id[@collection-id-type = 'publisher']">ERROR! The XML does not include the Series Acronym</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="collection-meta/collection-id/@collection-id-type">
            <sch:assert test=". = 'publisher'">ERROR! The only supported value here is "publisher"</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="collection-meta[@collection-type = 'series']">
            <sch:assert test="./title-group/title">ERROR! The XML does not include the Series Title</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="@institution-id-type">
            <sch:assert test=". = 'ringgold'">ERROR! Brill only supports Ringgold identifiers: @institution-id-type="ringgold"</sch:assert>
        </sch:rule>
    </sch:pattern>
    
        <!-- collection-meta sub-series -->
    <sch:pattern>
        <sch:rule context="@deceased|@equal-contrib">
            <sch:assert test=".='yes' or .='no'">Error! Only supported values are yes/no</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="collection-meta[@collection-type = 'sub-series']">
            <sch:assert test="./title-group/title">ERROR! The XML does not include the Sub-Series Title</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="collection-meta[@collection-type = 'sub-series']">
            <sch:assert test="./collection-id[@collection-id-type = 'publisher']">ERROR! The XML does not include the Series Acronym</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    
         <!-- /collection-meta sub-series -->
    <!-- /collection-meta -->
    
    <!-- book-meta -->
    
    <sch:pattern>
        <sch:rule context="book-meta//@contrib-type">
            <sch:assert  test=".='author' or .='volume editor'
                or .='contributor' or .='advisor' or .='editor' or .='editor/translator'
                or .='translator'">Invalid contrib-type <sch:value-of select="."/>. Values for the @contrib-type are: "author", "volume editor" (default Book-level editor), "contributor", "advisor", "editor" (used for Text Editions only), "editor/translator", "translator", "copy editor" </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-meta">
            <sch:assert test="custom-meta-group">book-meta must have custom-meta-group</sch:assert>
        </sch:rule>
        <sch:rule context="custom-meta-group">
            <sch:assert test="./custom-meta[((meta-name='version' and meta-value='fulltext') or (meta-name='version' and meta-value='header') and not((meta-name='version' and meta-value='fulltext') and (meta-name='version' and meta-value='header')))]">custom-meta/value pair version/fulltext or version/header must be contained</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-meta">
            <sch:assert test=".//book-title">ERROR! The XML does not include a Book title</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-meta">
            <sch:assert test=".//book-id[@book-id-type='doi']">ERROR! The XML does not include a Book DOI</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-meta">
            <sch:report test="not(./isbn[@publication-format='online'])">ERROR! The XML does not include an eISBN</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-meta">
            <sch:report test="not(./pub-date)">ERROR! The XML does not include a publication date</sch:report>
        </sch:rule>
        <sch:rule context="pub-date">
            <sch:report test="not(./year)">ERROR! This publication date does not have a year</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="isbn/@publication-format">
            <sch:assert test=".='print'
                or .='hardback'
                or .='paperback'
                or .='online'">ERROR! Unsupported ISBN format <sch:value-of select="."/>. Accepted values: "print"/"hardback"/"paperback"/"online"</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="isbn">
            <sch:assert test="matches(., '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]')">Error! ISBN must be 13 digits without hyphens</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern >
        <sch:rule context="book-meta">
            <sch:assert test="./permissions">ERROR! The XML does not contain a permissions section for the book</sch:assert>
        </sch:rule>
        <sch:rule context="book-meta//permissions/license[@license-type='open-access']">
            <sch:assert test="@xlink:title">ERROR! The XML does not contain the title of the CC license</sch:assert>
        </sch:rule>
        <sch:rule context="book-meta/permissions">
            <sch:assert test="./copyright-statement">ERROR! The XML does not contain a copyright statement for the book</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="supplementary-material">
            <sch:assert test="./@xlink:href">Error! @xlink:href must be provided.</sch:assert>
            <sch:assert test="./@specific-use">Error! @specific-use must be provided.</sch:assert>
        </sch:rule>
        <sch:rule context="supplementary-material/@specific-use">
            <sch:assert test=".='local' or .='figshare'">Error! @xlink:href must be provided.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="funding-group//institution-id">
            <sch:assert test="./@institution-id-type">Error! @institution-id-type required</sch:assert>
        </sch:rule>
        <sch:rule context="funding-group//institution-id/@institution-id-type">
            <sch:assert test=".='doi' or .='ringgold'">Error! Incorrect value for @institution-id-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-meta/permissions">
            <sch:assert test="./copyright-statement">Error! Copyright statement missing</sch:assert>
            <sch:assert test="./copyright-year">Error! Copyright year missing</sch:assert>
            <sch:assert test="./copyright-holder">Error! Copyright holder missing</sch:assert>
            <sch:assert test="./license">Error! License information missing</sch:assert>
            <sch:assert test="./license/license-p">Error! license-p missing</sch:assert>
        </sch:rule>
        <sch:rule context="book-meta/permissions/license">
            <sch:assert test="./@license-type">Error! @license-type missing</sch:assert>
        </sch:rule>
        <sch:rule context="book-meta/permissions/license/@license-type">
            <sch:assert test=".='ccc' or .='open-access'">Error! Incorrect value for @license-type</sch:assert>
        </sch:rule>
        <sch:rule context="book-meta/permissions/license[@license-type='open-access']">
            <sch:assert test="./@xlink:href">Error! @xlink:href missing</sch:assert>
        </sch:rule>
        <sch:rule context="book-meta/permissions/license[@license-type='open-access']/@xlink:title">
            <sch:assert test=".='CC-BY'
                or .='CC-BY-SA'
                or .='CC-BY-ND'
                or .='CC-BY-NC'
                or .='CC-BY-NC-SA'
                or .='CC-BY-NC-ND'">Error! Incorrect value for @xlink:title</sch:assert>
        </sch:rule>
    </sch:pattern>
    <!-- /book-meta -->
    <!-- book-part-meta -->
    <sch:pattern>
        <sch:rule context="book-body//book-part">
            <sch:assert test="./@book-part-type">Error! @book-part-type missing</sch:assert>
        </sch:rule>
        <sch:rule context="book-body//book-part">
            <sch:assert test="./@id">Error! @id missing</sch:assert>
        </sch:rule>
        <sch:rule context="book-body//book-part">
            <sch:assert test="./@seq">Error! @seq missing</sch:assert>
        </sch:rule>
        <sch:rule context="book-body//book-part/@book-part-type">
            <sch:assert test=".='part' or .='chapter'">Error! Incorrect value for @book-part-type. Should be 'part' or 'chapter'. 'section' is no longer in use.</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-part-meta//@contrib-type">
            <sch:assert test=".='author'
                or .='editor'
                or .='editor/translator'
                or .='translator'">Invalid contrib-type <sch:value-of select="."/>.  Brill supports the following contribution types for Chapter-level contributors: "author", "editor", "editor/translator", "translator"</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-part-meta//@alt-title-type">
            <sch:assert test=".='toc'
                or .='running-head'">Invalid alt-title-type <sch:value-of select="."/>. </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="book-body//book-part[@book-part-type!='part']/book-part-meta">
            <sch:assert test="fpage">Error! fpage not specified</sch:assert>
        </sch:rule>
        <sch:rule context="book-part-meta/fpage">
            <sch:assert test="./@specific-use and ./@specific-use=('PDF', 'pdf')">Error! @specific-use="PDF" required</sch:assert>
        </sch:rule>
        <sch:rule context="book-part-meta/fpage">
            <sch:assert test="./@seq">Error! @seq required</sch:assert>
        </sch:rule>
        <sch:rule context="book-part-meta/fpage/@seq">
            <sch:assert test="number(.) = number(.) and . > 0">Error! @seq must be numerical and > 0</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="self-uri">
            <sch:assert test="./@content-type">Error! @content-type must be specified</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="sec/@sec-type">
            <sch:assert test=".='heading-1'
                or .='heading-2'
                or .='heading-3'
                or .='heading-5'
                or .='heading-4'
                or .='heading-6'
                or .='heading-7'
                or .='heading-8'">Error! incorrect value for @sec-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="book-part-meta/permissions">
            <sch:assert test="./copyright-statement">Error! Copyright statement missing</sch:assert>
            <sch:assert test="./copyright-year">Error! Copyright year missing</sch:assert>
            <sch:assert test="./copyright-holder">Error! Copyright holder missing</sch:assert>
    <!--        <sch:assert test="./license">Error! License information missing</sch:assert>
            <sch:assert test="./license/license-p">Error! license-p missing</sch:assert> -->
        </sch:rule>
        <sch:rule context="book-part-meta/permissions/license">
            <sch:assert test="./@license-type">Error! @license-type missing</sch:assert>
        </sch:rule>
        <sch:rule context="book-part-meta/permissions/license/@license-type">
            <sch:assert test=".='ccc' or .='open-access'">Error! Incorrect value for @license-type</sch:assert>
        </sch:rule>
        <sch:rule context="book-part-meta/permissions/license[@license-type='open-access']">
            <sch:assert test="./@xlink:href">Error! @xlink:href missing</sch:assert>
        </sch:rule>
        <sch:rule context="book-part-meta/permissions/license[@license-type='open-access']/@xlink:title">
            <sch:assert test=".='CC-BY'
                or .='CC-BY-SA'
                or .='CC-BY-ND'
                or .='CC-BY-NC'
                or .='CC-BY-NC-SA'
                or .='CC-BY-NC-ND'">Error! Incorrect value for @xlink:href</sch:assert>
        </sch:rule>
    </sch:pattern>
   <!-- <sch:pattern>
        <sch:rule context="sec">
            <sch:assert test="./@id">Error! @id required</sch:assert>
        </sch:rule>
    </sch:pattern> -->
    <sch:pattern>
        <sch:rule context="title-group//label|book-title-group//label">
            <sch:report test="./xref">Error! xref must not be contained in label. Move it to the title instead.</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="xref">
            <sch:assert test="./@ref-type">Error! @ref-type required</sch:assert>
            <sch:assert test="./@rid">Error! @rid required</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="fn|table-wrap">
            <sch:assert test="./@id">Error! @id required</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="fig/@orientation|fig-group/@orientation">
            <sch:assert test=".='portrait' or .='landscape'">
                Error! Incorrect attribute value <sch:value-of select="."/> for @orientation. Must be "portrait" or "landscape"
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="fig/@position|fig-group/@position">
            <sch:assert test=".='anchor' or .='float' or .='margin'">
                Error! Incorrect attribute value <sch:value-of select="."/> for @position. Must be "achor", "float" or "margin"
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context='fig/@fig-type'>
            <sch:assert test=".='graphic'">
                Error! Incorrect attribute value <sch:value-of select="."/> for @fig.type. Must be "graphic"
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="graphic">
            <sch:assert test="./@xlink:href">Error! Link to graphic required</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="attrib">
            <sch:report test="./sc">Error! Do not use &lt;sc&gt; here as Small Caps style is part of the Brill Typographic Style and NOT semant</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern id="list">
        <sch:rule context="@list-type">
            <sch:assert test=".='order'
                or .='bullet'
                or .='alpha-lower'
                or .='alpha-upper'
                or .='roman-lower'
                or .='roman-upper'
                or .='simple'">Error! Invalid list-type <sch:value-of select="."/></sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern id="person-group">
        <sch:rule context="person-group">
            <sch:assert test="./@person-group-type">Error! @person-group-type required</sch:assert>
        </sch:rule>
        <sch:rule context="person-group/@person-group-type">
            <sch:assert test=".='all-authors'
                or .='assignee'
                or .='author'
                or .='compiler'
                or .='curator'
                or .='director'
                or .='editor'
                or .='guest-editor'
                or .='inventor'
                or .='transed'
                or .='translator'">Error! Incorrect value for @person-group-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
     <!--   <sch:rule context="mixed-citation">
            <sch:assert test="./@publication-type">Error! @publication-type missing</sch:assert>
        </sch:rule> -->
        <sch:rule context="mixed-citation/@publication-type">
            <sch:assert test=".='book'
                or .='journal'
                or .='web'
                or .='data'
                or .='list'
                or .='other'
                or .='patent'
                or .='thesis'
                or .='commun'
                or .='confproc'
                or .='discussion'
                or .='gov'">Error! Incorrect value for @publication-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="ref">
            <sch:assert test="./@id">Error! @id missing</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern
        >
        <sch:rule context="book-part-id[@book-part-id-type='doi']">
            <sch:assert test="substring-before(., '/') = substring-before(./ancestor-or-self::book/book-meta/book-id[@book-id-type='doi'], '/')">Error! Chapter DOI must have the same prefix as book DOI</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="nav-pointer">
            <sch:assert test="./@rid">Error! @rid required</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern id="refs">
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./fpage) &lt;= 1">Error! Use only /fpage per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./lpage) &lt;= 1">Error! Use only /lpage per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./volume) &lt;= 1">Error! Use only /volume per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./article-title) &lt;= 1">Error! Use only /article-title per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./series) &lt;= 1">Error! Use only /series per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./issue) &lt;= 1">Error! Use only /issue per mixed-citation to avoid Crossref-Errors</sch:assert>
        </sch:rule>
    </sch:pattern>
</sch:schema>