<?xml version="1.0" encoding="UTF-8"?>

<!-- ====================================================================
	TThis Schematron is used to check JATS files against Brill's the specific requirements laid out on https://confluence.brill.com/display/BE/XML+and+PDF+instructions. To be used by Brill CDE.
	Copyright (C) 2021 Koninklijke Brill NV, Leiden, Netherlands.
	
	Brill JATS-Checker v1.0 © Brill / Johannes Gottschalt, April 2022
	-->
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
    xmlns:xlink="http://www.w3.org/1999/xlink">

    <sch:ns uri="http://www.w3.org/1999/xlink" prefix="xlink"/>
    <sch:ns prefix="ali" uri="http://www.niso.org/schemas/ali/1.0/"/> 
    <!-- General checks -->
    <sch:pattern>
        <sch:rule context="@article-type">
            <sch:assert see="https://confluence.brill.com/display/BE/JATS+instructions#JATSinstructions-ArticleMetadata"
                test=". = 'abstract' or . = 'other' or . = 'addendum' or . = 'announcement' or . = 'article-commentary' or . = 'book-review' or . = 'books-received' or . = 'brief-report' or . = 'calendar' or . = 'case-report' or . = 'collection' or . = 'correction' or . = 'discussion' or . = 'dissertation' or . = 'editorial' or . = 'in-brief' or . = 'introduction' or . = 'letter' or . = 'meeting-report' or . = 'news' or . = 'obituary' or . = 'oration' or . = 'partial-retraction' or . = 'product-review' or . = 'rapid-communication' or . = 'reply' or . = 'reprint' or . = 'research-article' or . = 'retraction' or . = 'review-article' or . = 'translation'"
                >Incorrect article-type <sch:value-of select="."/>.</sch:assert>
        </sch:rule>
        <sch:rule context="article[@article-type='correction' or @article-type='editorial' or @article-type='introduction' or @article-type='obituary' 
            or @article-type='partial-retraction' or @article-type='retraction']">
            <sch:assert test="//article-meta//ali:free_to_read">Article type is <sch:value-of select="."/>. The article must therefore be free. Add ali:free_to_read to permissions</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article">
            <sch:report test="not(@xml:lang)">Error! Article language must be specified in root element.</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article">
            <sch:report test="not(@dtd-version = '1.0' or @dtd-version = '1.1')">Error! Root element must contain @dtd-version="1.1"</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="//contrib-id[@contrib-id-type = 'orcid']">
            <sch:report test="not(@authenticated) or ./@authenticated != 'true'">orcid must have
                @authenticated="true"</sch:report>
            <sch:assert
                test="matches(., 'https://orcid.org/[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9X]')"
                >Incorrect Orcid-Syntax.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="title-group">
            <sch:report test="count(./subtitle) > 1">Only one subtitle per title-group allowed. Use
                &lt;break/&gt; for line break in subtitle</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="trans-title-group">
            <sch:report test="count(./trans-subtitle) > 1">Only one trans-subtitle per
                trans-title-group allowed. Use &lt;break/&gt; for line break in
                subtitle</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="trans-title-group">
            <sch:report see="https://www.loc.gov/standards/iso639-2/php/code_list.php" test="not(@xml:lang)">Error! Language in trans-title-group must be specified</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="//*/@*">
            <sch:report test=". = ''">Empty attribute. Leave out if not required or fill correct
                value</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="counts">
            <sch:report test="matches(@count, '[0-9]+')">counts/@count must be
                numerical</sch:report>
        </sch:rule>
    </sch:pattern>

    <sch:pattern>
        <sch:rule context="name/@name-style">
            <sch:report test=". != 'eastern' and . != 'western' and . != 'given-only'">@name-style
                must be 'eastern', 'western' or 'given-only'</sch:report>
        </sch:rule>
    </sch:pattern>

    <!-- /General checks -->
    

    <!-- journal-meta -->
    
    <sch:pattern>
        <sch:rule context="journal-meta">
            <sch:assert test="./journal-id[@journal-id-type='eissn']">Error! journal-meta must contain journal-id[@journal-id-type='eissn']</sch:assert>
            <sch:assert test="matches(./journal-id[@journal-id-type='eissn'], '[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9X]')">Error! Incorrect ISSN-Syntax <sch:value-of select="."/>. Make sure to contain hyphen.</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="journal-meta">
            <sch:assert test="./journal-id[@journal-id-type='publisher-id']">Error! Journal ID not specified</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="journal-meta">
            <sch:assert test=".//journal-title">Error! Journal title not specified</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="journal-meta">
            <sch:assert test=".//abbrev-journal-title">Error! Abbreviated journal title not specified</sch:assert>
        </sch:rule>
        <sch:rule context="abbrev-journal-title">
            <sch:report test="not(./@abbrev-type and ./@abbrev-type='ltwa')">Error! @abbrev-type="ltwa": required attribute/value pair.</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="issn">
            <sch:assert test="./@publication-format">Error! /issn/@publication-format not specified</sch:assert>
            <sch:assert test="matches(., '[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9X]')">Error! Incorrect ISSN-Syntax <sch:value-of select="."/>. Must be dddd-dddd.</sch:assert>
        </sch:rule>
        <sch:rule context="issn/@publication-format">
            <sch:assert test=". = 'print' or .='online'">Error! /issn/@publication-format must be 'print' or 'online'</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="publisher/@speficic-use">
            <sch:assert test=". = 'print' or .='online'">Error! /publisher/@speficic-use must be 'print' or 'online'</sch:assert>
        </sch:rule>
    </sch:pattern>
    <!-- /journal-meta -->
    <!-- article-meta -->
    
    <sch:pattern>
        <sch:rule context="article-meta">
            <sch:assert test="./article-id[@pub-id-type='doi']">Error! Article DOI not specified</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule
            context="article-id[@pub-id-type='doi']">
            <sch:assert test="matches(., '^10.\d{4,9}/[-._;()/:a-z0-9]+$')"> DOI pattern incorrect</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-id">
            <sch:assert test="./@pub-id-type">Error! article-id/@pub-id-type is required</sch:assert>
        </sch:rule>
        <sch:rule context="article-id/@pub-id-type">
            <sch:assert test=".='doi' or .='EM'">Error! Incorrect value <sch:value-of select="."/>. Must be 'doi' or 'EM', where EM is the Editorial Manager manuscript number. Always include if provided.</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="article-meta">
            <sch:assert test="./title-group/article-title">Error! Article title not specified</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta//title-group">
            <sch:assert test="./alt-title[@alt-title-type='toc']">Error! Title group must contain &lt;alt-title alt-title-type="toc"&gt; </sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="alt-title">
            <sch:assert test="./@alt-title-type">Error! @alt-title-type not specified</sch:assert>
        </sch:rule>
        <sch:rule context="alt-title/@alt-title-type">
            <sch:assert test=".='toc' or .='running-head'">Error! Incorrect attribute-value <sch:value-of select="."/>. Must be 'toc' or 'running-head'</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="contrib">
            <sch:assert test="./@contrib-type">Error! @contrib-type not specified</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta//contrib/@contrib-type">
            <sch:assert test=".='author' or .='translator' or .='editor'">Error! Incorrect attribute value <sch:value-of select="."/>. Must be "author", "translator", "editor".</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta//contrib/@corresp">
            <sch:assert test=".='yes'">Incorrect attribute value <sch:value-of select="."/>. Only accepted value here is "yes". Leave out otherwise</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta//contrib/@deceased">
            <sch:assert test=".='no' or .='yes'">Incorrect attribute value <sch:value-of select="."/>. Accepted values are "yes" and "no".</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta//contrib/@equal-contrib">
            <sch:assert test=".='no' or .='yes'">Incorrect attribute value <sch:value-of select="."/>. Accepted values are "yes" and "no".</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta//institution-id">
            <sch:assert test="./@institution-id-type = 'ringgold'">Must have @institution-id-type = 'ringgold'</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta">
            <sch:assert test=".//pub-date[@publication-format = 'online']">Error! @publication-format: Req. "online"</sch:assert>
        </sch:rule>
        <sch:rule context="article-meta">
            <sch:assert test=".//pub-date[@date-type = 'article']">Error! Must contain pub-date[@date-type = 'article']</sch:assert>
        </sch:rule>
        <sch:rule context="article-meta[//issue]">
            <sch:assert test=".//pub-date[@date-type = 'issue']">Error! Must contain pub-date[@date-type = 'issue'] if the article has been assigned to an issue</sch:assert>
        </sch:rule>
        <sch:rule context="article-meta//pub-date/@date-type">
            <sch:assert test=". = 'issue' or .='article'">Incorrect attribute value <sch:value-of select="."/>. Accepted value are "issue" and "article".</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="pub-date/year">
            <sch:assert test=" matches(., '[0-9][0-9][0-9][0-9]')">Error! Year must contain of 4 digits</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta/fpage">
            <sch:assert test="./@specific-use and ./@specific-use=('CAP', 'PDF')">Error! @specific-use="PDF" or @specific-use="CAP" required</sch:assert>
            <sch:assert test="./@seq">Error! @seq required</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="supplementary-material">
            <sch:assert test="./@xlink:href">Error! @xlink:href must be provided.</sch:assert>
            <sch:assert test="./@specific-use">Error! @specific-use must be provided.</sch:assert>
        </sch:rule>
        <sch:rule context="supplementary-material/@specific-use">
            <sch:assert test=".='local' or .='figshare'">Error! @xlink:href must be provided.</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta/history/date">
            <sch:assert test="./@date-type">Error! @date-type required</sch:assert>
        </sch:rule>
        <sch:rule context="article-meta/history/date/@date-type">
            <sch:assert test=".='received'
                or .='initial-decision'
                or .='rev-recd'
                or .='rev-request'
                or .='accepted'
                or .='version-of-record'
                or .='pub'
                or .='preprint'
                or .='retracted'
                or .='corrected'">Incorrect attribute value for @date-type.</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta/permissions">
            <sch:assert test="./copyright-statement">Error! Copyright statement missing</sch:assert>
            <sch:assert test="./copyright-year">Error! Copyright year missing</sch:assert>
            <sch:assert test="./copyright-holder">Error! Copyright holder missing</sch:assert>
            <sch:assert test="./license">Error! License information missing</sch:assert>
            <sch:assert test="./license/license-p">Error! license-p missing</sch:assert>
        </sch:rule>
        <sch:rule context="article-meta/permissions/license">
            <sch:assert test="./@license-type">Error! @license-type missing</sch:assert>
        </sch:rule>
        <sch:rule context="article-meta/permissions/license/@license-type">
            <sch:assert test=".='ccc' or .='open-access'">Error! Incorrect value for @license-type</sch:assert>
        </sch:rule>
        <sch:rule context="article-meta/permissions/license[@license-type='open-access']">
            <sch:assert test="./@xlink:href">Error! @xlink:href missing</sch:assert>
            <sch:assert test="./@xlink:title">Error! @xlink:href missing</sch:assert>
        </sch:rule>
        <sch:rule context="article-meta/permissions/license[@license-type='open-access']/@xlink:title">
            <sch:assert test=".='CC-BY'
                or .='CC-BY-SA'
                or .='CC-BY-ND'
                or .='CC-BY-NC'
                or .='CC-BY-NC-SA'
                or .='CC-BY-NC-ND'">Error! Incorrect value for @xlink:href</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta/self-uri">
            <sch:assert test="./@xlink:href">Error! @xlink:href missing</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta/trans-abstract">
            <sch:assert test="./@xml:lang">Error! @xml:lang missing</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta/kwd-group">
            <sch:assert test="./@kwd-group-type">Error! @kwd-group-type missing</sch:assert>
            <sch:assert test="./@kwd-group-type='uncontrolled'">Error! Incorret value for  @kwd-group-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="funding-group//institution-id">
            <sch:assert test="./@institution-id-type">Error! @institution-id-type required</sch:assert>
        </sch:rule>
        <sch:rule context="funding-group//institution-id/@institution-id-type">
            <sch:assert test=".='doi' or .='ringgold'">Error! Incorrect value for @institution-id-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="article-meta">
            <sch:assert test="custom-meta-group">book-meta must have custom-meta-group</sch:assert>
        </sch:rule>
        <sch:rule context="custom-meta-group">
            <sch:assert test="./custom-meta[((meta-name='version' and meta-value='fulltext') or (meta-name='version' and meta-value='header') and not((meta-name='version' and meta-value='fulltext') and (meta-name='version' and meta-value='header')))]">custom-meta/value pair version/fulltext or version/header must be contained</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="body//graphic">
            <sch:assert test="./@orientation='portrait' or ./@orientation='landscape'">Error! Incorrect attribute value for @orientation</sch:assert>
        </sch:rule>
        <sch:rule context="body//graphic">
            <sch:assert test="./@xlink:href">Error! @xlink:href required</sch:assert>
        </sch:rule>
    </sch:pattern>
    <!-- /article-meta -->
    <!-- body -->
    
    <sch:pattern>
        <sch:rule context="body//graphic/@position">
            <sch:assert test="./@orientation">Error! @orientation required</sch:assert>
            <sch:assert test=".='anchor' or .='float' or .='margin'">Error! Incorrect attribute value for @orientation</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="ext-link">
            <sch:assert test="./@ext-link-type">Error! @ext-link-type missing</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="target|fn">
            <sch:assert test="./@id">Error! @id missing</sch:assert>
        </sch:rule>
    </sch:pattern>
    <!-- /body -->
    
    <!-- sec -->
    
    <sch:pattern>
        <sch:rule context="sec/@sec-type">
            <sch:assert test=".='heading-1'
               or .='heading-2'
               or .='heading-3'
               or .='heading-5'
               or .='heading-4'
               or .='heading-6'
               or .='heading-7'
               or .='heading-8'">Error! incorrect value for @sec-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="fig-group//attrib|verse-group//attrib">
            <sch:report test="./sc">Error! Do not use &lt;sc&gt; here as Small Caps style is part of the Brill Typographic Style and NOT semant</sch:report>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="@list-type">
            <sch:assert test=".='order'
                or .='bullet'
                or .='alpha-lower'
                or .='alpha-upper'
                or .='roman-lower'
                or .='roman-upper'
                or .='simple'">Error! Invalid list-type <sch:value-of select="."/></sch:assert>
        </sch:rule>
    </sch:pattern>
    <!-- /sec -->
    <!-- ref-list -->
    <sch:pattern>
        <sch:rule context="person-group">
            <sch:assert test="./@person-group-type">Error! @person-group-type required</sch:assert>
        </sch:rule>
        <sch:rule context="person-group/@person-group-type">
            <sch:assert test=".='all-authors'
                or .='assignee'
                or .='author'
                or .='compiler'
                or .='curator'
                or .='director'
                or .='editor'
                or .='guest-editor'
                or .='inventor'
                or .='transed'
                or .='translator'">Error! Incorrect value for @person-group-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
     <!--   <sch:rule context="mixed-citation">
            <sch:assert test="./@publication-type">Error! @publication-type missing</sch:assert>
        </sch:rule> -->
        <sch:rule context="mixed-citation/@publication-type">
            <sch:assert test=".='book'
                or .='journal'
                or .='web'
                or .='data'
                or .='list'
                or .='other'
                or .='patent'
                or .='thesis'
                or .='commun'
                or .='confproc'
                or .='discussion'
                or .='gov'">Error! Incorrect value for @publication-type</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="ref|media">
            <sch:assert test="./@id">Error! @id missing</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <!-- /ref-list -->
    <!-- media -->
    <sch:pattern>
        <sch:rule context="media">
            <sch:assert test="./@content-type and ./@content-type='fighsare'">Error! @content-type="figshare" required.</sch:assert>
            <sch:assert test="./@specific-use and ./@specific-use='original-format'">Error! @specific-use="original-format" required.</sch:assert>
            <sch:assert test="./@position and ./@position='anchor'">Error! @position="anchor" required.</sch:assert>
            <sch:assert test="./@orientation and ./@orientation='portrait'">Error! @orientation="portrait" required</sch:assert>
            <sch:assert test="./@mimetype and ./@mimetype='video'">Error! @mimetype="video" required.</sch:assert>
            <sch:assert test="./@mime-subtype and ./@mime-subtype='avi'">Error! @mime-subtype="avi" required</sch:assert>
            <sch:assert test="./@xlink:href">Error! @xlink:href required.</sch:assert>
            <sch:assert test="./object-id">Error! /object-id required.</sch:assert>
        </sch:rule>
    </sch:pattern>
    <sch:pattern>
        <sch:rule context="object-id">
            <sch:assert test="./@pub-id-type and ./@pub-id-type='doi'">Error! @pub-id-type="doi" required.</sch:assert>
            <sch:assert test="./@specific-use and ./@specific-use='metadata'">Error! @specific-use="metadata" required</sch:assert>
            
        </sch:rule>
    </sch:pattern>
    <!-- /media -->
    <sch:pattern id="refs">
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./fpage) &lt;= 1">Error! Use only /fpage per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./lpage) &lt;= 1">Error! Use only /lpage per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./volume) &lt;= 1">Error! Use only /volume per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./article-title) &lt;= 1">Error! Use only /article-title per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./series) &lt;= 1">Error! Use only /series per mixed-citation to avoid Crossref errors</sch:assert>
        </sch:rule>
        <sch:rule context="mixed-citation">
            <sch:assert test="count(./issue) &lt;= 1">Error! Use only /issue per mixed-citation to avoid Crossref-Errors</sch:assert>
        </sch:rule>
    </sch:pattern>
    
    
</sch:schema>
